
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class RecipeTest {

    @BeforeClass
    public static void setUP(){
        RestAssured.baseURI = "http://virtserver.swaggerhub.com/4uck/whats-cook_res_tfull_api/1.0.0";
    }

    @Test
    public void test_success_get_by_jsonSchema() {
        File file = new File("src/test/java/json/schemaListRecipe.json");

        RestAssured.basePath = "/recipe/1";
                get().
                then().
                assertThat().body(matchesJsonSchema(file));

    }

    @Test
    public void test_success_post_create_recipe() {

        RestAssured.basePath = "/recipe";

        Map<String, String> recipe = new HashMap<String, String>();
        recipe.put("name", "sup");
        recipe.put("description","sup sup sup");
        recipe.put("recipe_id","3");

        Response response =
                given().
                        log().all().
                        contentType("application/json").
                        accept("application/json").
                        body(recipe).
                when().
                        log().all().
                        post().
                then().
                        log().all().
                        extract().response();

        Assert.assertTrue(response.statusCode() == 200);
    }

    @Test
    public void test_success_get_recommend_recipe() {

        RestAssured.basePath = "/recipe/recommend";

        File file = new File("src/test/java/json/schemaRecomendRecipe.json");

        Map user_id = new HashMap();
        user_id.put("uuid", "550e8400-e29b-41d4-a716-446655440000");

                given().
                        contentType("application/json").
                        accept("application/json").
                        body(user_id).
                when().
                        get().
                then().
                        assertThat().body(matchesJsonSchema(file));

    }

    @Test
    public void test_success_get_recipe_id() {
        RestAssured.basePath = "/recipe/1";

        Response response =
                given().
                when().
                        get().
                then().
                        extract().
                        response();

        System.out.println(response.asString());

        Assert.assertTrue(response.statusCode() == 200);
    }

//    @Test
//    public void test_bad_recipe_id() {
//
//    }

    @Test
    public void test_success_delete_remove_recipe() {

        RestAssured.basePath = "/recipe/1";

        Response response =
                given().
                        when().
                        delete().
                then().
                        extract().
                        response();

        Assert.assertTrue(response.statusCode() == 200);
    }

//    @Test
//    public void test_bad_remove_reciepe() {
//
//    }

    @Test
    public void test_success_put_update_recipe() {

        RestAssured.basePath = "/recipe/1";

        Map<String, String> recipe = new HashMap<String, String>();
        recipe.put("title", "Доширак");

        Response response =
                given().
                        contentType("application/json").
                        accept("application/json").
                        body(recipe).
                when().
                        put().
                then().
                        extract().
                        response();

        Assert.assertTrue(response.path("title").equals("Плов"));
    }

    //    @Test
    //    public void test_bad_update_reciepe() {
    //
    //    }
}
